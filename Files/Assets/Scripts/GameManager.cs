﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// Di sini akan diimplementasikan apabila permainan telah berakhir baik itu menang atau kalah
public class GameManager : MonoBehaviour {
  public GameObject completeLevelUI;
  public GameObject failedLevelUI;
  [HideInInspector] public int enemyShooted = 0;

  public void AddShooted(){
    enemyShooted = enemyShooted + 1;
    Debug.Log("Musuh tertembak 1, tersisa" + (18-enemyShooted).ToString());

    if(enemyShooted == 18){
      Debug.Log("Tertembak semua");
      CompleteLevel(); // Apabila jumlah yang tertembak sudah semua (18 objek) tampilkan menang
    }    
  }
  public void CompleteLevel(){
    // Fungsi yang dijalankan apabila menang;
    Debug.Log("Permainan Selesai, Menang!");
    completeLevelUI.SetActive(true);
  }
  public void EndLevel(){
    // Fungsi yang dijalankan apabila kalah;
    Debug.Log("Permainan berakhir karena kalah");
    failedLevelUI.SetActive(true);
  }
}