﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary{
  public float xMin, xMax, zMin, zMax;
}

public class PlayerBehaviour : MonoBehaviour {
  public float planeSpeed;
  public Boundary boundary;

  void Update(){
    // Memanggil fungsi menembak laser dari script LaserBehaviour
    GetComponent<LaserBehaviour>().ShootLaser();
  }

  void FixedUpdate(){
    // Melakukan update terhadap fisik agar pesawat dapat bergerak

    float moveHorizontal = Input.GetAxis("Horizontal");
    float moveVertical = Input.GetAxis("Vertical");

    Vector3 playerMovement = new Vector3(moveHorizontal,0.0f,moveVertical);
    GetComponent<Rigidbody>().velocity = playerMovement * planeSpeed;

    GetComponent<Rigidbody>().position = new Vector3(
      Mathf.Clamp(GetComponent<Rigidbody>().position.x, boundary.xMin, boundary.xMax),
      0.0f,
      Mathf.Clamp(GetComponent<Rigidbody>().position.z, boundary.zMin, boundary.zMax)
    );
  }
}