﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour{
  // Bagian yang akan diimplementasikan saat menekan tombol PLAY NOW untuk mulai private void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info) {
  
  public void StartGame(){
    Debug.Log("Memulai Permainan!");
    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
  }
  public void QuitGame(){
    Debug.Log("Keluar dari Permainan!");
    Application.Quit();
  }
}