﻿using UnityEngine;
using UnityEngine.UI;

// Untuk mengaplikasikan mengambil dan mengupdate data seberapa banyak musuh tersisa.
public class EnemiesLeft : MonoBehaviour
{
  public Text enemiesLeftText;

  void Update(){
    // Mengupdate tulisan dengan penambahan skor, setiap musuh bernilai 10 poin
    enemiesLeftText.text = "Enemies Left: " + (18 - FindObjectOfType<GameManager>().enemyShooted).ToString();
  }
}
