﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRandomRotation : MonoBehaviour{
  public float tumble;
  
  void Start(){
    GetComponent<Rigidbody>().angularVelocity = Random.insideUnitCircle * tumble;
  }
}