﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour {

  public Rigidbody enemy;
  private float fallSpeed;

  void Start(){
    fallSpeed = Random.Range(-0.05f, -0.01f); // Memberikan kecepatan jatuh yang random
  }

  /*
  FUNGSI INI untuk membuat musuh jatuh mendekati pemain. Saya ingin membuat yang sedikit out of the box untuk ini
  Saya tidak mengikuti cuplikan video untuk bagian mendekati pemain. Ini FITUR, bukan bug :v
   */
  
  void FixedUpdate(){
    enemy.AddForce(0,0,fallSpeed);
  }

  // Fungsi untuk menghandle apabila musuh bersentuhan dengan suatu objek, seperti melewati batas atau terkena pemain
  void OnTriggerEnter(Collider other){
    // Menghancurkan musuh apabila tag yang tersentuh musuh adalah laser
    if (other.tag == "Laser"){
      // Apabila yang menyentuh adalah laser, maka laser dan musuh hilang
      Debug.Log("Laser menyentuh musuh, menghancurkan laser...");
      Destroy(other.gameObject);
      Destroy(gameObject);
      FindObjectOfType<GameManager>().AddShooted(); // Menambahkan kedalam counter berapa banyak musuh tertembak
    }
    else if (other.tag == "Player" || other.tag == "Boundary"){
      // Melakukan handle apabila pesawat bertabrakan dengan musuh
      Debug.Log("Pesawat menabrak musuh atau musuh melewati batas, mengakhiri permainan...");
      Destroy(other.gameObject);
      Destroy(gameObject);
      FindObjectOfType<GameManager>().EndLevel(); // Memunculkan UI kekalahan
    }
    else{
      return ;
    }

  }
}