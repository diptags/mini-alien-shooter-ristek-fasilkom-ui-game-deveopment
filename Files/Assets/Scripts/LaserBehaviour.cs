﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserBehaviour : MonoBehaviour {

  public GameObject shot;
  public Transform shotSpawn;
  public float fireRate;
  private float nextFire;

  public void ShootLaser(){
    // Menembak laser dari pesawat

    if ( (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space)) && Time.time > nextFire){
      Debug.Log("Menembak laser dari pesawat");

      nextFire = Time.time + fireRate;
      GameObject shotInstance;
      shotInstance = Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
      shotInstance.GetComponent<Rigidbody>().AddForce(0,0,1000);
    
      /* 
      Untuk menghancurkan laser, saya membuat script tambahan, DestroyLaser.cs.
      DestroyLaser.cs berada pada 3D object tambahan yang bernama Boundary.
      Alasannya adalah, agar mempermudah pemeriksaan collision saja.
      */
    }
  }


    //TODO : Implement Interaksi Laser-Enemy
}
