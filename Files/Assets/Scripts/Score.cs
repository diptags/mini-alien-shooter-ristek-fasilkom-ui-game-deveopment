﻿using UnityEngine;
using UnityEngine.UI;

// File untuk sistem skoring
public class Score : MonoBehaviour{
  public Text scoreText;

  void Update(){
    // Mengupdate tulisan dengan penambahan skor, setiap musuh bernilai 10 poin
    scoreText.text = "Your Score: " + (FindObjectOfType<GameManager>().enemyShooted * 10).ToString();
  }
}