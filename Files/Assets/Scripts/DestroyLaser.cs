﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
Fungsi untuk menghancurkan laser apabila telah melewati batas camera
Sistemnya dengan cara memberikan batas berupa cube yang rendernya dimatikan, kemudiaan
apabila laser menyentuh cube, maka akan menjadi trigger untuk menghapus objek laser

Penghapusan laser diperlukan agar objek laser yang telah dibuat tidak menumpuk di hierarcy bar
*/

public class DestroyLaser : MonoBehaviour{
  void OnTriggerExit(Collider other){
    Destroy(other.gameObject);
    Debug.Log("Laser melewati batas, laser hancur!");
  }
}